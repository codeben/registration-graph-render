import * as xml2js from "xml2js";
import { ParsedXMLModel } from "./models/parsed-xml.model";
import { Chart, ChartConfiguration } from "chart.js";
import { ChartOptions } from "chart.js";
import { DataTypes } from "./models/data-type.enum";

const Parser = new xml2js.Parser;
const DATA_TYPES = DataTypes;

let yearColours: string[] = [
    '#ffc400',
    '#d91c1c',
    '#18c1db',
    '#f7e307',
    '#62de2c'
];

let files: FileList;
let readXml: string[] = [];
let jsFromXml: ParsedXMLModel[] = [];
let totals: { [key: string]: number };
let readFileItteration: number = 0;
let keyName: DataTypes = DATA_TYPES.PercentPresent;
let labelName: string = 'Percent Present';

const options: ChartOptions = {
    legend: {
        display: false,
    },
    tooltips: {
        enabled: false,
    },
    responsive: false,
    events: [],
    animation: {
        duration: 0,
        onComplete: function () {
            var ctx = this.chart.ctx;
            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
            ctx.fillStyle = "black";
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
    
            this.data.datasets.forEach((dataset: any) => 
            { 
                for (var i = 0; i < dataset.data.length; i++) {
                    for(var key in dataset._meta)
                    {
                        var model = dataset._meta[key].data[i]._model;
                        ctx.fillText(dataset.data[i] + '%', model.x, (dataset.data[i] > 1) ? model.y + 20 : model.y - 5);
                    }
                }
            });
        },
    },
}

const prepareData = (): void => {
    jsFromXml.forEach((rawData) => {
        const labels: string[] = [...rawData.report.SelectedBrowserGroups[0].$.BrowserGroups.split('+'), 'Totals'];
        const normalData: number[] = rawData.report.BaseGroups[0].GroupType[0].Group.map((group) => {
            return parseInt(group.$[keyName], 10);
        });

        const totalKeyName: string = 'Total' + keyName;
        const totalData: number = parseInt(((rawData.report.BaseGroups[0].GroupType[0].GrandTotals[0].$ as any)[totalKeyName] as string), 10);
        const data = [...normalData, totalData];

        const year: string = labels[0].replace(/[A-Za-z]/g, '');
        const backgroundColorIndex: number = parseInt(year, 10) - 7;
        const backgroundColor: string = yearColours[backgroundColorIndex];
        const chartElement = document.createElement('canvas');
        const label: string = `Year ${year} - ${labelName}`;

        const fromDate = rawData.report.Parameters[0].$.FromDate;
        const toDate = rawData.report.Parameters[0].$.ToDate;

        const customOptions: ChartOptions = customOptionsBuilder(data);

        const headerTitle = label + ' - ' + fromDate + ' - ' + toDate;
        const domElements = domBuilder(headerTitle);

        new Chart(
            domElements[1],
            buildChartConfig(customOptions, data, labels, label, backgroundColor)
        );

        if (jsFromXml.length > 1) {
            totalize(new Chart(chartElement, buildChartConfig(customOptions, data, labels, label, backgroundColor)), year);
        }
    });

    if (jsFromXml.length > 1) {
        const domElements = domBuilder('Totals');
        const totalizedData = formatTotalizedData();
        new Chart(
            domElements[1],
            buildChartConfig(
                customOptionsBuilder(totalizedData[1]),
                totalizedData[1],
                totalizedData[0],
                'Total', 
                '#ff0000',
            )
        );
    }
    document.getElementById('create-graphs').setAttribute('disabled', 'true');
}

const customOptionsBuilder = (data: number[]): ChartOptions => {
    const min = Math.min(...data);
    const max = Math.max(...data);
    const suggestedMin: number = (min >= 1) ? min - 1 : min;
    const suggestedMax: number = (min <= 99) ? min + 1 : max;

    return {
        ...options,
        scales: {
            yAxes: [{
                ticks: {
                    suggestedMin,
                    suggestedMax
                }
            }],
        },
    }
}

const buildChartConfig = (
    customOptions: any,
    data: number[],
    labels: string[],
    label: string,
    backgroundColor: string
): ChartConfiguration => {
    return {
        type: 'bar',
        options: customOptions,
        data: {
            labels,
            datasets: [{
                data,
                label,
                backgroundColor,
            }],
        },
    }
}

const domBuilder = (title: string): [HTMLHeadingElement, HTMLCanvasElement] => {
    const titleElem = document.createElement('h1')
    titleElem.textContent = title;
    titleElem.style.pageBreakBefore = 'always';
    document.body.appendChild(titleElem);

    const chartElement = document.createElement('canvas');
    chartElement.id = `chart-${btoa(Date.now().toString())}`;
    chartElement.width = 800;
    chartElement.height = 400;
    document.body.appendChild(chartElement);

    return [
        titleElem,
        chartElement
    ];
}

const formatTotalizedData = (): [string[], number[]] => {
    return [
        Object.keys(totals),
        Object.values(totals)
    ];
}

const totalize = (config: ChartConfiguration, year: string): void => {
    const data: number[] = (config.data.datasets[0].data as number[]);
    totals = { ...totals, [year]: data[data.length - 1] };

    const averageTotal: number = Math.round(Object.keys(totals).reduce((accumulator: number, key: string) => 
            accumulator + (totals as any)[key], 0) / Object.keys(totals).length)

    totals['Combined'] = averageTotal
}

const checkIfXml = (file: File): boolean => {
    return !!(file.type.match('text/xml'));
}

const convertFileStringToJs = (): void => {
    readXml.forEach(xml => {
        Parser.parseString(xml, (error: any, result: ParsedXMLModel) => {
            if (!error && result) { jsFromXml = [...jsFromXml, result]; } 
            else { console.error(error); }
        });
    });

    Parser.reset();
}

const readFile = (file: File, mappedFilesLength: number): void => {
    const reader: FileReader = new FileReader();
    reader.onload = () => {readXml = [...readXml, reader.result.toString()]};
    reader.readAsText(file);
    reader.onloadend = () => {
        if (readFileItteration === mappedFilesLength) {
            convertFileStringToJs();
        } else {
            readFileItteration += 1;
        }
    };
}

const getFiles = (): void => {
    files = null;
    files = (document.getElementById('xml-upload') as HTMLInputElement).files;
}

const reset = (): void => {
    readXml = [];
    jsFromXml = [];
    totals = {};
    readFileItteration = 0;
}

const handleFileSelect = (): void => {
    clearDom();
    reset();
    getFiles();
    mapAndReadFiles();
    document.getElementById('data-type').removeAttribute('disabled');
    document.getElementById('create-graphs').removeAttribute('disabled');
}

const mapAndReadFiles = (): void => {
    const mappedFiles = Array.from(files).reduce((acc, file) => [...acc, ...(checkIfXml(file)) ? [file] : []], []);
    mappedFiles.forEach((xmlFile: File) => readFile(xmlFile, mappedFiles.length - 1));
}

const clearDom = (): void => {
    Array.from(document.querySelectorAll('canvas, h1')).forEach((canvas) => {
        canvas.remove();
    });
}

const selectKeyName = (event: Event): void => {
    document.getElementById('data-type').removeAttribute('disabled');
    document.getElementById('create-graphs').removeAttribute('disabled');
    keyName = ((event.target as HTMLSelectElement).value as DataTypes);
    labelName = (event.target as HTMLSelectElement).selectedOptions[0].textContent;
}

document.getElementById('data-type')
    .addEventListener(
        'change',
        selectKeyName,
        false,
    );

document.getElementById('xml-upload')
    .addEventListener(
        'change',
        handleFileSelect,
        false,
    );

document.getElementById('create-graphs')
    .addEventListener(
        'click',
        prepareData,
        false
    );

document.getElementById('clear')
    .addEventListener(
        'click',
        clearDom,
        false
    );