export enum DataTypes {
    PercentPresent = 'PercentPresent',
    PercentAEA = 'PercentAEA',
    PercentAuthorised = 'PercentAuthorised',
    PercentUnauthorised = 'PercentUnauthorised',
    PercentAttend = 'PercentAttend',
    PercentLate = 'PercentLate',
}

export enum TotalDataTypes {
    TotalPercentPresent = 'PercentPresent',
    TotalPercentAEA = 'PercentAEA',
    TotalPercentAuthorised = 'PercentAuthorised',
    TotalPercentUnauthorised = 'PercentUnauthorised',
    TotalPercentAttend = 'PercentAttend',
    TotalPercentLate = 'PercentLate',
}