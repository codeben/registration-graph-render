export interface ParsedXMLModel {
    report: Report;
}

export interface Report {
    $: $;
    Parameters ? : (ParametersEntity)[] | null;
    SelectedBrowserGroups ? : (SelectedBrowserGroupsEntity)[] | null;
    BaseGroups ? : (BaseGroupsEntity)[] | null;
}

export interface $ {
    source: string;
}

export interface ParametersEntity {
    $: $1;
}

export interface $1 {
    ReportTitle: string;
    FromDate: string;
    ToDate: string;
    Scope: string;
    ShowBoysGirlsSeparate: string;
    ShowDataAsNumbers: string;
}

export interface SelectedBrowserGroupsEntity {
    $: $2;
}

export interface $2 {
    BrowserGroups: string;
}

export interface BaseGroupsEntity {
    GroupType ? : (GroupTypeEntity)[] | null;
}

export interface GroupTypeEntity {
    $: $3;
    Group ? : (GroupEntity)[] | null;
    MissingMarks ? : (MissingMarksEntity)[] | null;
    GrandTotals ? : (GrandTotalsEntity)[] | null;
}

export interface $3 {
    Type: string;
}

export interface GroupEntity {
    $: $4;
}

export interface $4 {
    BaseGroupID: string;
    BaseGroupName: string;
    Gender: string;
    Present: string;
    AEA: string;
    Authorised: string;
    Unauthorised: string;
    Possible: string;
    PercentPresent: string;
    PercentAEA: string;
    PercentAuthorised: string;
    PercentUnauthorised: string;
    PercentPossible: string;
    PercentAttend: string;
    PercentLate: string;
}

export interface MissingMarksEntity {
    $: $5;
}

export interface $5 {
    MissingMarkCount: string;
}

export interface GrandTotalsEntity {
    $: $6;
}

export interface $6 {
    TotalPresent: string;
    TotalAEA: string;
    TotalAuthorised: string;
    TotalUnauthorised: string;
    TotalPossible: string;
    TotalLate: string;
    TotalPercentPresent: string;
    TotalPercentAEA: string;
    TotalPercentAuthorised: string;
    TotalPercentUnauthorised: string;
    TotalPercentPossible: string;
    TotalPercentAttend: string;
    TotalPercentLate: string;
}